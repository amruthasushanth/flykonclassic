import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { CompanyService } from '../../../@core/data/company.service';


@Component({
  selector: 'company-edit',
  templateUrl: './company-edit.component.html',
  styleUrls: ['./company-edit.component.scss']
})
export class CompanyEditComponent implements OnInit {

  editcompanyForm: FormGroup;
  company=[];
  update_company=[];
  company_address=[];
  company_location=[];
  update_id: number;

  constructor(private companyService: CompanyService) { 
    this.createForm();
    this.getCompany();
    this.update_id = this.companyService.update_id;
  }

  ngOnInit() {
  }
  createForm() {
    this.editcompanyForm = new FormGroup({
      name: new FormControl('',Validators.required),
      address: new FormControl('',Validators.required),
      city: new FormControl('',Validators.required),
      location: new FormControl('',Validators.required),
      mobile: new FormControl('',[
        Validators.required,
        Validators.pattern("^[0-9]{10}"),
        Validators.minLength(8),
      ]),

    })

  }


  update(data){
    console.log(data);
    this.companyService.update(data,this.update_id).subscribe(res=>{
      console.log('res--',res);
    });

  }

  getCompany(){
    this.companyService.getCompany().subscribe((res:any)=>{
      console.log('inside get Lead after subscribe--->res',res);
      let company = res.stores;
      // console.log("updatecompany-----------",company);
      for (let i = 0; i < company.length; i++) {
        // const element = company[i];
        console.log("getcompany------- edit---",company[i])
        if (company[i].id == this.update_id) {

          this.update_company = company[i];
          this.company_address = company[i].address;
          // this.company_location = this.company_address.location;

          
          console.log("updatecompany-----------",this.update_company);
          console.log("updatecompanyaddress-----------",this.company_address);
          
        }

        
        
      }
       
      });

  }

}
