import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { NbToastrService } from '@nebular/theme';
import { Router } from '@angular/router';
import { CompanyService } from '../../../@core/data/company.service';

@Component({
  selector: 'company-add',
  templateUrl: './company-add.component.html',
  styleUrls: ['./company-add.component.scss']
})
export class CompanyAddComponent implements OnInit {

  addcompanyForm: FormGroup;
  id: any;

  constructor(private service:CompanyService,private toastrService: NbToastrService,private router: Router) {
    this.createForm();
   }

  ngOnInit() {
  }

  createForm() {
    this.addcompanyForm = new FormGroup({
      name: new FormControl('',Validators.required),
      address: new FormControl('',Validators.required),
      city: new FormControl('',Validators.required),
      location: new FormControl('',Validators.required),
      mobile: new FormControl('',[
        Validators.required,
        Validators.pattern("^[0-9]{10}"),
        Validators.minLength(8),
      ]),

    })

  }

  // save(addcompanyForm,id){
  //   console.log(addcompanyForm);
  //   this.service.getCompany().subscribe((res:any)=>{
  //     console.log('save inside company add',res.id);
  //     id=res.id;
  //     this.service.addCompany(addcompanyForm,id).subscribe(res=>{
  //       console.log('res--',res,id);
  //       // this.toastrService.show(status, `Data saved successfully!`, { status:'success' });
  //       // this.createForm();
  //       // this.router.navigate(['pages/leads']);
  
  //     })

  // })
  // }

  addStore(addcompanyForm){
    console.log('inside leadForm---save--',this.addcompanyForm.value);
    this.service.addStore(addcompanyForm).subscribe(res=>{
      console.log('res--',res);
      this.toastrService.show(status, `Data saved successfully!`, { status:'success' });
      this.createForm();
      this.router.navigate(['pages/company']);

    },(err=>{
      console.log('Error--',err);
      this.toastrService.show(status, `Found Error`, { status:'danger' });

    }));
  }

  cancel(){
    this.router.navigate(['pages/company']);
  }
}
