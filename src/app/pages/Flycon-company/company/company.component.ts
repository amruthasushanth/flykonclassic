import { Component, OnInit, TemplateRef } from '@angular/core';
import { Router } from '@angular/router';

import { NbDialogService } from '@nebular/theme';
import { Company } from '../../../@core/data/company';
import { CompanyService } from '../../../@core/data/company.service';


@Component({
  selector: 'company',
  templateUrl: './company.component.html',
  styleUrls: ['./company.component.scss']
})
export class CompanyComponent implements OnInit {

  p: number = 1;
  searchText: [];
  company: any = [];
  head_company: any = [];
  company_address: any = [];
  company_id: number;
  delete_storeid: number;
  store_id = [];
  new_company = new Company();


  constructor(private router: Router, private service: CompanyService, private dialogService: NbDialogService) {


    // this.getCompany();
    this.getMain();
  }

  ngOnInit() {
    this.getCompany();
    this.getMain();

  }

  getCompany() {
    console.log('inside get Lead', this.company);
    this.service.getCompany().subscribe((res: any) => {
      console.log('inside get Lead after subscribe--->res', res);
      this.company = res.stores;
      let store_id = [];
      console.log('company array-res-users-', res.stores);
      this.company.forEach(element => {
        console.log('element',element);
        element.users.forEach(x => {
          console.log('x',x);
          store_id.push(x.storeId)
          // x.users.forEach(y => {
          //   console.log('x',x);
          //   store_id.push(y.storeId)
          // })
    console.log('store_id',store_id);

        })
      });


    })
  }

  getMain() {
    this.service.getCompany().subscribe((res: any) => {
      console.log('inside get Lead after subscribe--->res', res);
      this.head_company = res;
      this.company_address = this.head_company.address.text;
      this.service.company_id = res.id;

      console.log('headcompany array-res--', res.name);

    })


  }

  // addCompany(id,data) {
  //   console.log('inside add_company()--');
  //   this.router.navigate(['pages/add-company']);
  //   this.service.addCompany(id,data).subscribe(res => {
  //     console.log('res--', res);
  //   })
  // }


  addStore(){
    console.log('inside add_lead()--');
    this.router.navigate(['pages/add-company']);
    
  }

  edit(update_id) {
    this.router.navigate(['pages/edit-company']);
    this.service.update_id=update_id; 

    console.log('inside edit with _id', update_id);
  }


  open(dialog: TemplateRef<any>, id,company_id) {
    console.log('open delete box', id);
    this.dialogService.open(dialog, { context: 'Are you sure want to delete?' })
    this.delete_storeid = id;
    this.company_id = company_id;
    console.log('VALUES inside DELETE------------------', id,company_id);

  }


  delete() {
    console.log('inside delete----1');

    this.service.delete(this.company_id,this.delete_storeid).toPromise().then(()=>{
      this.router.navigate(['pages/company']);
    })
    


  }





}
