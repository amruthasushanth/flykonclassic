import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { PagesComponent } from './pages.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { FlyconLeadComponent } from './flycon-lead/flycon-lead.component';
import { AddLeadComponent } from './add-lead/add-lead.component';
import { EditLeadComponent } from './edit-lead/edit-lead.component';
import { DeleteLeadComponent } from './delete-lead/delete-lead.component';
import { LeadViewComponent } from './lead-view/lead-view.component';
import { FlyconUsersComponent } from './Users/flycon-users/flycon-users.component';
import { AddUsersComponent } from './Users/add-users/add-users.component';
import { CompanyComponent } from './Flycon-company/company/company.component';
import { CompanyAddComponent } from './Flycon-company/company-add/company-add.component';
import { CompanyEditComponent } from './Flycon-company/company-edit/company-edit.component';
import { AuthGuard } from '../framework/auth/services/flycon_auth/auth.guard';
import { FlyconCustomersComponent } from './Customers/flycon-customers/flycon-customers.component';
import { FlyconCustomerAddComponent } from './Customers/flycon-customer-add/flycon-customer-add.component';
import { FlyconCustomerEditComponent } from './Customers/flycon-customer-edit/flycon-customer-edit.component';

const routes: Routes = [{
  path: '',
  component: PagesComponent,
  children: [
    {
      path: 'dashboard',
      component: DashboardComponent,
      canActivate:[AuthGuard]
    },
    {
      path:'leads',
      component:FlyconLeadComponent,
      canActivate:[AuthGuard]
    },
    {
      path:'add-lead',
      component:AddLeadComponent,
      canActivate:[AuthGuard]
    },
    {
      path:'edit-lead',
      component:EditLeadComponent,
      canActivate:[AuthGuard]
    },
    {
      path:'customers',
      component:FlyconCustomersComponent,
      // canActivate:[AuthGuard]
    },
    {
      path:'add-customer',
      component:FlyconCustomerAddComponent,
      // canActivate:[AuthGuard]
    },
    {
      path:'edit-customer',
      component:FlyconCustomerEditComponent,
      // canActivate:[AuthGuard]
    },

    {
      path:'company',
      component:CompanyComponent,
      canActivate:[AuthGuard]
    },
    {
      path:'add-company',
      component:CompanyAddComponent,
      canActivate:[AuthGuard]
    },
    {
      path:'edit-company',
      component:CompanyEditComponent,
      canActivate:[AuthGuard]
    },
    {
      path:'view/id',
      component:LeadViewComponent,
      canActivate:[AuthGuard]
    },
    {
      path:'flycon_users',
      component:FlyconUsersComponent,
      canActivate:[AuthGuard]
    },
    {
      path:'add-users',
      component:AddUsersComponent,
      canActivate:[AuthGuard]
    },
    {
      path: '',
      redirectTo: 'dashboard',
      pathMatch: 'full',
    },
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PagesRoutingModule {
}
