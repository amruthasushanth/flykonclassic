import { Component, OnInit, TemplateRef } from '@angular/core';
import { FlyconUserService } from '../../../@core/data/flycon-users.service';
import { Router } from '@angular/router';
import { NbDialogService, NbToastrService } from '@nebular/theme';


@Component({
  selector: 'flycon-users',
  templateUrl: './flycon-users.component.html',
  styleUrls: ['./flycon-users.component.scss']
})
export class FlyconUsersComponent implements OnInit {

  p: number = 1;
  searchText:[];
  view:any;
  users:any=[
  //   {
  //   "name":"Amrutha",
  //   "store":"K.C",
  //   "username":"Ammu",
  //   "mobile":"9497899788"
  // },
  // {
  //   "name":"Devu",
  //   "store":"T.K.stores",
  //   "username":"deu",
  //   "mobile":"8497899788"
  // },
  // {
  //   "name":"Test",
  //   "store":"MP",
  //   "username":"asdf",
  //   "mobile":"8495899788"
  // }
];


user_id:number;
id:number;


 store_id :any =[];
  

  constructor(private service : FlyconUserService,private router:Router,private dialogService: NbDialogService, private toastrService: NbToastrService) { }

  ngOnInit() {
    this.getUser();
  }

   
getUser(){
  console.log('inside get Users',this.users);
  this.service.get_Users().subscribe((res:any)=>{
    console.log('inside get Users after subscribe--->res',res);
    this.users=res;
    console.log('Users array-res--',res);
    // this.users.forEach(element => {
    //   this.store_id.push(element.storeId)
    // });
    // console.log('store_id-----',this.store_id);

  })
}

add_users(){
  let store_id = [];
  console.log('inside USERS--add()--',this.users);
  this.users.forEach(element => {
      store_id.push(element.storeId)
    });
    console.log('store_id-----',store_id);

  this.router.navigate(['pages/add-users']);
  
}

open(dialog: TemplateRef<any>,id) {
  console.log('open delete box with id---',id);
  this.dialogService.open(dialog, { context: 'Are you sure want to delete?' });
  // this.users.forEach(element => {
  //   this.user_id.push(element.id)
  // });
  this.user_id = id;
  // console.log('id---------------',this.user_id);
  
}

delete(){
  console.log('inside delete----',this.user_id);
  this.service.delete( this.user_id).subscribe((res:any)=>{
    console.log('res----',res);
    this.toastrService.show(status, `Data deleted successfully!`, { status:'success' });
    // ref.close();
    this.router.navigate(['pages/flycon_users']);

  },(err=>{
    console.log('Error--',err);
    this.toastrService.show(status, `Found Error`, { status:'danger' });

  }));
    // ref.close();                                                                        
  
}



}
