import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FlyconCustomersComponent } from './flycon-customers.component';

describe('FlyconCustomersComponent', () => {
  let component: FlyconCustomersComponent;
  let fixture: ComponentFixture<FlyconCustomersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FlyconCustomersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FlyconCustomersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
