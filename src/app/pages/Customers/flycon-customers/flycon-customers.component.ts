import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FlyconCustomerService } from '../../../@core/data/flycon-customer.service';

@Component({
  selector: 'flycon-customers',
  templateUrl: './flycon-customers.component.html',
  styleUrls: ['./flycon-customers.component.scss']
})
export class FlyconCustomersComponent implements OnInit {

  p: number = 1;
  searchText:[];
  view:any;
  customer_data:any=[];
  customerValue = 0;

  constructor(private services : FlyconCustomerService,private router:Router ) { }

  ngOnInit() {
    // this.getAllCustomers();
  }

  // getAllCustomers(){
  //   console.log('All Customers inside customer ts--');
  //   this.services.getCustomers(this.customerValue, 10).subscribe((res:any)=>{
  //     console.log('inside get Customers after subscribe--->res',res);
  //     this.customer_data=res;
  //     console.log('Users array-res--',res);
  //   })
  // }

}
