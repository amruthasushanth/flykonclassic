import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FlyconCustomerAddComponent } from './flycon-customer-add.component';

describe('FlyconCustomerAddComponent', () => {
  let component: FlyconCustomerAddComponent;
  let fixture: ComponentFixture<FlyconCustomerAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FlyconCustomerAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FlyconCustomerAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
