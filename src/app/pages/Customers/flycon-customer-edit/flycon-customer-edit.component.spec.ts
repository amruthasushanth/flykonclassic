import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FlyconCustomerEditComponent } from './flycon-customer-edit.component';

describe('FlyconCustomerEditComponent', () => {
  let component: FlyconCustomerEditComponent;
  let fixture: ComponentFixture<FlyconCustomerEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FlyconCustomerEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FlyconCustomerEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
