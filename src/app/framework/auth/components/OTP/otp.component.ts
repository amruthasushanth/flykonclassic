/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { Component, Inject } from '@angular/core';
import { Router } from '@angular/router';
import { NB_AUTH_OPTIONS } from '../../auth.options';
import { getDeepFromObject } from '../../helpers';

import { NbAuthService } from '../../services/auth.service';
import { NbAuthResult } from '../../services/auth-result';
import { AuthenticationService } from '../../services';

@Component({
  selector: 'nb-otp-page',
  styleUrls: ['./otp.component.scss'],
  template: `
    <nb-auth-block>
      <h2 class="title">OTP Verification</h2>
      <small class="form-text sub-title" >A One Time Password has been sent to<h6> {{ email }}</h6>.Please enter the OTP below to verify your Email Address.
      If you cannot see the email from "Flycon" in your inbox,make sure to check your SPAM folder.</small>
      <form (ngSubmit)="submitOtp()" #otpForm="ngForm">

        <div *ngIf="showMessages.error && errors && errors.length > 0 && !submitted"
             class="alert alert-danger" role="alert">
          <div><strong>Oh snap!</strong></div>
          <div *ngFor="let error of errors">{{ error }}</div>
        </div>
        <div *ngIf="showMessages.success && messages && messages.length > 0 && !submitted"
             class="alert alert-success" role="alert">
          <div><strong>Hooray!</strong></div>
          <div *ngFor="let message of messages">{{ message }}</div>
        </div>

     

        <div class="form-group">
          <label for="input-otp" class="sr-only">Enter your OTP</label>
          <input name="otp" [(ngModel)]="user.otp" id="input-otp" #otp="ngModel"
                 class="form-control" placeholder="One Time Password" 
                 [class.form-control-danger]="otp.invalid && otp.touched"
                 [required]="getConfigValue('forms.validation.otp.required')"
                 autofocus>
          <small class="form-text error" *ngIf="otp.invalid && otp.touched && otp.errors?.required">
            otp is required!
          </small>
         
        </div>

        <button [disabled]="submitted || !otpForm.form.valid" class="btn btn-hero-success btn-block"
                [class.btn-pulse]="submitted">
          Verify
        </button>
      </form>

      <!--<div class="links col-sm-12">
        <small class="form-text">
          Already have an account? <a routerLink="../login"><strong>Sign In</strong></a>
        </small>
        <small class="form-text">
          <a routerLink="../register"><strong>Sign Up</strong></a>
        </small>
      </div>-->
    </nb-auth-block>
  `,
})
export class NbOtpComponent {

  redirectDelay: number = 0;
  showMessages: any = {};
  strategy: string = '';

  submitted = false;
  errors: string[] = [];
  messages: string[] = [];
  user: any = {};
  email:any;
 

  constructor(protected service: NbAuthService,
              @Inject(NB_AUTH_OPTIONS) protected options = {},
              protected router: Router,protected flyAuth: AuthenticationService) {

                this.email = JSON.parse(localStorage.getItem("email"));
    
                //console.log("email--------------------",this.email);

    this.redirectDelay = this.getConfigValue('forms.otp.redirectDelay');
    this.showMessages = this.getConfigValue('forms.otp.showMessages');
    this.strategy = this.getConfigValue('forms.otp.strategy');
  }

  submitOtp(): void {
      //console.log('testing OTP');
      
    this.errors = this.messages = [];
    this.submitted = true;
    this.flyAuth.otpVerify(this.email,this.user.otp).subscribe((result:any)=>{
      this.submitted=false;
      if(result.validUser==true){
        this.messages = ['Successfully verified OTP. '];
        this.showMessages = {     // show/not show success/error messages
          success: true,
          error: false,
      }
      //console.log('result----------------------------------',result);
        
        localStorage.setItem('auth_token', JSON.stringify(result));
        setTimeout(() => {
          //console.log("timout called");
            this.router.navigateByUrl('auth/reset-password');
            //console.log('this.user',this.user);
            
           localStorage.setItem("email", JSON.stringify(this.email));
           localStorage.setItem("otp", JSON.stringify(this.user.otp));
           
          }, 1000);
        }
        else{
          this.submitted = false;
          // console.log("error",err);
          this.showMessages = {     // show/not show success/error messages
            success: false,
            error: true,
          }
          this.errors = ['Invalid OTP..!'];
        }
        //console.log("response====================>", result);
  
      }, (err: any) => {
       
      
    })
  }

    // this.service.requestPassword(this.strategy, this.user).subscribe((result: NbAuthResult) => {
    //   this.submitted = false;
    //   if (result.isSuccess()) {
    //     this.messages = result.getMessages();
    //   } else {
    //     this.errors = result.getErrors();
    //   }

    //   const redirect = result.getRedirect();
    //   if (redirect) {
    //     setTimeout(() => {
    //       return this.router.navigateByUrl(redirect);
    //     }, this.redirectDelay);
    //   }
    // });
  

  getConfigValue(key: string): any {
    return getDeepFromObject(this.options, key, null);
  }
}
