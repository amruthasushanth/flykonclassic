import { Injectable, TemplateRef } from '@angular/core';
import { HttpHeaders, HttpParams, HttpClient } from '@angular/common/http';
import { CredentialService } from './credential.service';
import 'rxjs/add/operator/map';
import { NbDialogService } from '@nebular/theme';
import { Store } from './company';


@Injectable()
export class CompanyService {
    token: any;
    userId: any;
    deleteId:number;
    update_id:any;
    update_company = [];
    company_id:any;
    store = new Store();
    constructor(private http: HttpClient, private credential: CredentialService,private dialogService: NbDialogService) {
      this.token = JSON.parse(localStorage.getItem('access_token')).token
      console.log("token ++++", this.token);
    }


    addStore(data){
      const headers = new HttpHeaders()
      .set('Content-Type', 'application/json; charset=utf-8')
      .set("Authorization", "Bearer " + this.token);
      const uri = this.credential.dev_ip+"/tenants/api/v1/company/" + this.company_id + "/stores";
      console.log("service",data,'this.company_id--------',this.company_id);
      let obj = {
        // id: this.store.id,
        name: data.name,
        description: "",
        address: {
          // id: this.selectedItem.address.id,
          city: data.city,
          text: data.address,
          location: {
            // id: this.selectedItem.address.location.id,
            name: data.location,
          }
        },
        contactNumber: data.mobile,
        // socialAddresses: [],
        // users: this.store.users
      };
      console.log("Service addStore",uri,obj);
      return this.http.post(uri,obj,{ headers: headers });
    }


    getCompany(){
      console.log('inside service---getCompany');

      const headers = new HttpHeaders()
      .set('Content-Type', 'application/x-www-form-urlencoded; charset=utf-8')
      .set("Authorization", "Bearer" + this.token);
      const uri = this.credential.dev_ip+'/tenants/api/v1/company';
      console.log("company management URI",uri)
      return this
              .http
              .get(uri,{ headers: headers })
              .map(res => {
                console.log('uri----url', uri);
                console.log('res getCompany service-------------------',res);
                return res;
                
              });
      
    }



    delete(id,store_id){
      const headers = new HttpHeaders()
      .set('Content-Type', 'application/x-www-form-urlencoded; charset=utf-8')
      .set("Authorization", "Bearer" + this.token);
      console.log("inside delete------------",id,'store_id----------------',store_id);
      const uri = this.credential.dev_ip+"/tenants/api/v1/company/" +
      id +
      "/stores/" +
      store_id
      ;
console.log('id-----',id,'store_id-----',store_id,'<<<<<<<<<<<<-----------uri---->>>',uri);

      return this.http.delete(uri,{ headers: headers })
      .map(res=>{
        return res;
      });

    }
  

    // update(data,id){

    //   const uri = this.credential.dev_ip+"tenants/api/v1/company/" +id+ "/stores/";
    //   console.log("inside companyservice--------------",id,data)

    //   return this.http.put(uri,data);


    // }

    update(data,id){
      const headers = new HttpHeaders()
      .set('Content-Type', 'application/json; charset=utf-8')
      .set("Authorization", "Bearer " + this.token);
      const uri = this.credential.dev_ip+"/tenants/api/v1/company/" +id+ "/stores/";
      console.log("service",data,'this.id--------',id);
      let obj = {
        // id: this.store.id,
        name: data.name,
        // description: "",
        address: {
          // id: this.selectedItem.address.id,
          city: data.city,
          text: data.address,
          location: {
            // id: this.selectedItem.address.location.id,
            name: data.location,
          }
        },
        contactNumber: data.mobile,
        // socialAddresses: [],
        // users: this.store.users
      };
      console.log("Service addStore",uri,obj);
      return this.http.put(uri,obj,{ headers: headers });
    }


    // addStore(addcompanyForm){
    //   // const headers = new HttpHeaders()
    //   // .set('Content-Type', 'application/x-www-form-urlencoded; charset=utf-8')
    //   // .set("Authorization", "Basic " + this.token);
    //   const uri = this.credential.dev_ip+"/tenants/api/v1/company/" + this.company_id + "/stores";
    //   console.log("service",addcompanyForm);
    //   const obj = {
    //     name:addcompanyForm.name,
    //     address:addcompanyForm.address,
    //     city:addcompanyForm.city,
    //     location:addcompanyForm.location,
    //     contactNumber:addcompanyForm.mobile
    //   };
    //   console.log("Service addLead",obj);
    //   return this.http.post(uri,obj/*,{ headers: headers }*/)
    //   // .subscribe(res => console.log('Done addLead post'));
    // }\

  }